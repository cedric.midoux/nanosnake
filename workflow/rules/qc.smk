rule nanoplot:
    input:
        summary=lambda wildcards: "DATA/{run}/sequencing_summary.txt",
    output:
        report="results/qc/nanoplot/{run}/NanoPlot-report.html",
        stats="results/qc/nanoplot/{run}/NanoStats.txt",
    params:
        outdir=lambda wildcards, output: os.path.dirname(output.report),
    threads: 4
    # log: "logs/qc/nanoplot-{run}.log"
    benchmark:
        "logs/benchmark/qc/nanoplot-{run}.tsv"
    shell:
        """
        ulimit -n 2048
        conda activate nanoplot-1.38.1
        NanoPlot --threads {threads} --outdir {params.outdir} --barcoded --summary {input.summary}
        conda deactivate
        """


# rule minionqc:
# 	input:
# 		summary = SUMMARY.loc['sequencing_summary_file'].reset_index().apply(lambda row: "DATA/{index}/{sequencing_summary_file}".format(**row), axis=1)
# 	output:
# 		rscript = "resources/script/MinIONQC.R",
# 		summary = "results/qc/minionqc/sequencing_summary.txt",
# 		report = "results/qc/minionqc/DATA/summary.yaml"
# 	params:
# 		outdir = lambda wildcards, output: os.path.dirname(os.path.dirname(output.report))
# 	threads: 4
# 	log: "logs/qc/minionqc.log"
# 	benchmark: "logs/benchmark/qc/minionqc.tsv"
# 	shell:
# 		"""
# 		wget https://raw.githubusercontent.com/roblanf/minion_qc/master/MinIONQC.R -O {output.rscript} > {log}
# 		cat {input.summary} > {output.summary}
# 		Rscript {output.rscript} --input {output.summary} --output {params.outdir} --processors {threads} > {log}
# 		"""


rule fastqc:
    input:
        "results/preprocess/porechop/{sample}.fastq.gz",
    output:
        html="results/qc/fastqc/{sample}.html",
        zip="results/qc/fastqc/{sample}_fastqc.zip",
    params:
        "--quiet",
    log:
        "logs/qc/fastqc/{sample}.log",
    threads: 4
    wrapper:
        "v1.3.2/bio/fastqc"


rule multiqc:
    input:
        expand("results/qc/fastqc/{sample}_fastqc.zip", sample=SAMPLES),
    output:
        "results/qc/multiqc/multiqc.html",
    log:
        "logs/qc/multiqc.log",
    wrapper:
        "v1.3.2/bio/multiqc"
