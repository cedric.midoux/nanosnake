rule gtdb:
    input:
        wait=rules.pool_mag.output,
    output:
        expand(
            "results/annotation/gtdb-{{approaches}}/gtdbtk.{db}.summary.tsv",
            db=["bac120", "ar53"],
        ),
    params:
        genomes="results/binning/drep-{approaches}/mag/",
        outdir=lambda wildcards, output: os.path.dirname(output[0]),
        min=10,
    threads: 48
    log:
        "logs/annotation/gtdb-{approaches}.log",
    benchmark:
        "logs/benchmark/annotation/gtdb-{approaches}.tsv"
    shell:
        """
        conda activate gtdbtk-2.2.3
        gtdbtk classify_wf --genome_dir {params.genomes} --out_dir {params.outdir} --extension fasta --skip_ani_screen --min_perc_aa {params.min} --cpus {threads} --force &> {log}
        conda deactivate
        """


rule gtdb_combine:
    input:
        summary=expand(
            "results/annotation/gtdb-{{approaches}}/gtdbtk.{db}.summary.tsv",
            db=["bac120", "ar53"],
        ),
    output:
        tsv="results/annotation/gtdb-{approaches}/gtdbtk.summary.tsv",
    threads: 1
    benchmark:
        "logs/benchmark/annotation/gtdb_combine-{approaches}.tsv"
    run:
        import pandas as pd

        pd.concat(
            [pd.read_table(file, index_col=0) for file in input.summary], axis=0
        ).to_csv(output.tsv, sep="\t")
