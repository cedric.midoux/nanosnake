rule jgi:
    input:
        bam="results/align_postpolish/{sample}/{sample}.bam",
    output:
        depth="results/binning/metabat2/depth/{sample}.txt",
    threads: 1
    log:
        "logs/binning/metabat2-depth/{sample}.log",
    benchmark:
        "logs/benchmark/binning/metabat2-depth/{sample}.tsv"
    shell:
        """
        conda activate metabat2-2.15
        jgi_summarize_bam_contig_depths --outputDepth {output.depth} {input.bam} &> {log}
        conda deactivate
        """


checkpoint metabat2:
    input:
        contigs="results/assembly/consensus/{sample}.fasta",
        # depth = "results/binning/metabat2/depth/{sample}.txt"
    output:
        dir=directory("results/binning/metabat2/{sample}/"),
        cluster="results/binning/metabat2/{sample}/{sample}",
    params:
        minContig=1500,
        maxEdges=500,
    threads: 8
    log:
        "logs/binning/metabat2/{sample}.log",
    benchmark:
        "logs/benchmark/binning/metabat2/{sample}.tsv"
    shell:
        """
        conda activate metabat2-2.15
        metabat2 --inFile {input.contigs} --outFile {output.dir}/{wildcards.sample} --minContig {params.minContig} --maxEdges {params.maxEdges} --numThreads {threads} --saveCls --unbinned --verbose &> {log}
        conda deactivate
        """


# rule maxbin2:
# 	input:
# 		contigs = "results/assembly/consensus/{sample}.fasta",
# 		fastq = "results/preprocess/porechop/{sample}.fastq.gz"
# 	output:
# 		cluster = directory("results/binning/maxbin2/{sample}")
# 	params:
# 		min_contig_length = 1500
# 		prob_threshold = 0.9
# 	threads: 8
# 	log: "logs/binning/maxbin2/{sample}.log"
# 	benchmark: "logs/benchmark/binning/maxbin2/{sample}.tsv"
# 	shell:
# 		"""
# 		conda activate maxbin2-2.2.7
# 		run_MaxBin.pl -contig {input.contigs} --reads {input.fastq} -out {output.cluster}/{wildcards.sample} -min_contig_length {params.min_contig_length} -prob_threshold {params.prob_threshold} -thread {threads} -verbose
# 		conda deactivate
# 		"""


wildcard_constraints:
    approaches="per-sample|all-approaches|pool-samples|pool-consensus",


def aggregate_genomes(wildcards):
    if wildcards.approaches == "per-sample":
        bins = SAMPLES
    elif wildcards.approaches == "all-approaches":
        bins = SAMPLES + ["pool-samples", "pool-consensus"]

    samples_list = []
    for sample in bins:
        metabat2_output = checkpoints.metabat2.get(sample=sample).output["dir"]
        samples_list.extend(
            expand(
                "results/binning/metabat2/{sample}/{sample}.{nb}.fa",
                sample=sample,
                nb=glob_wildcards(
                    os.path.join(metabat2_output, "{sample}.{i,\d+}.fa")
                ).i,
            )
        )
    return samples_list


# dRep v2.3.2 66 -comp 50 -con 10 dereplicated the MAGs at 99% ANI clustering to indicate the number of distinct lineages
# and overlap of likely strains between WWTPs, and at 95% ANI to indicate the number of distinct species.
checkpoint drep:
    input:
        genomes=aggregate_genomes,
    output:
        dir=directory("results/binning/drep-{approaches}/"),
        genomes=directory("results/binning/drep-{approaches}/dereplicated_genomes/"),
        widb="results/binning/drep-{approaches}/data_tables/Widb.csv",
    params:
        # completeness = 50,
        # contamination = 10,
        completeness=0,
        contamination=10,
        P_ani=0.95,
        S_ani=0.99,
    threads: 24
    log:
        "logs/binning/drep-{approaches}.log",
    benchmark:
        "logs/benchmark/binning/drep-{approaches}.tsv"
    shell:
        """
        conda activate drep-3.2.2
        dRep dereplicate --S_algorithm fastANI --P_ani {params.P_ani} --S_ani {params.S_ani} --completeness {params.completeness} --contamination {params.contamination} --genomes {input.genomes} --processors {threads} {output.dir} &> {log}
        conda deactivate
        """


localrules:
    stats_mag_temp,
    stats_mag_names,
    mag_rename,
    pool_mag,


rule stats_mag_temp:
    input:
        genome="results/binning/drep-{approaches}/dereplicated_genomes/",
    output:
        temp("results/binning/drep-{approaches}/temp_stats_mag.tsv"),
    params:
        list_mag=lambda wildcards, input: __import__("glob").glob(
            "{}/*.fa".format(input.genome)
        ),
    threads: 1
    benchmark:
        "logs/benchmark/binning/drep-{approaches}.temp_stats_mag.tsv"
    shell:
        """
        conda activate seqkit-2.0.0
        seqkit stats {params.list_mag} --tabular --threads {threads} > {output}
        conda deactivate
        """


checkpoint stats_mag_names:
    input:
        table="results/binning/drep-{approaches}/temp_stats_mag.tsv",
    output:
        table="results/binning/drep-{approaches}/stats_mag.tsv",
    threads: 1
    benchmark:
        "logs/benchmark/binning/drep-{approaches}.stats_mag.tsv"
    run:
        import pandas as pd

        df = pd.read_table(input.table)
        df["mag"] = "mag_" + df.sum_len.rank(ascending=False, method="first").astype(
            "int"
        ).astype("str").str.zfill(3)
        df.to_csv(output.table, sep="\t", index=False)


def MAGs(wildcards):
    return (
        pd.read_table(checkpoints.stats_mag_names.get(**wildcards).output["table"])
        .mag.sort_values()
        .unique()
        .tolist()
    )


rule mag_rename:
    input:
        table="results/binning/drep-{approaches}/stats_mag.tsv",
    output:
        "results/binning/drep-{approaches}/mag/{mag}.fasta",
    params:
        mag_path=lambda wildcards, input: __import__("pandas").read_table(input.table).set_index("mag").file[wildcards.mag],
    threads: 1
    benchmark:
        "logs/benchmark/binning/magrename/{approaches}-{mag}.tsv"
    shell:
        """
        conda activate seqkit-2.0.0
        seqkit replace --pattern '.+' --replacement '{wildcards.mag}_{{nr}}' --nr-width 5 {params.mag_path} > {output}
        conda deactivate
        """


def list_mag(wildcards):
    return expand(
        "results/binning/drep-{{approaches}}/mag/{mag}.fasta", mag=MAGs(wildcards)
    )


checkpoint contigs_mag:
    input:
        fasta="results/binning/drep-{approaches}/mag/{mag}.fasta",
    output:
        txt="results/binning/drep-{approaches}/mag/{mag}.txt",
    threads: 1
    benchmark:
        "logs/benchmark/binning/contigs_mag/{approaches}-{mag}.tsv"
    shell:
        """
        conda activate seqkit-2.0.0
        seqkit seq --name {input.fasta} --out-file {output.txt}
        conda deactivate
        """


rule pool_mag:
    input:
        list_mag,
    output:
        "results/binning/drep-{approaches}/pool-mag.fasta",
    threads: 1
    benchmark:
        "logs/benchmark/binning/{approaches}-pool-mag.tsv"
    shell:
        """
        conda activate seqkit-2.0.0
        seqkit seq {input} --threads {threads} > {output}
        conda deactivate
        """


rule checkm2:
    input:
        pool="results/binning/drep-{approaches}/pool-mag.fasta",
    output:
        report="results/binning/checkm2-{approaches}/quality_report.tsv",
    params:
        mag=lambda wildcards: "results/binning/drep-{}/mag/".format(
            wildcards.approaches
        ),
        outdir=lambda wildcards, output: os.path.dirname(output.report),
    threads: 12
    log:
        "logs/binning/checkm2/{approaches}.log",
    benchmark:
        "logs/benchmark/binning/checkm2/{approaches}.tsv"
    shell:
        """
        conda activate checkm2-0.1.3
        checkm2 predict --threads 12 --extension .fasta --input {params.mag} --output-directory {params.outdir} &> {log}
        conda deactivate
        """
