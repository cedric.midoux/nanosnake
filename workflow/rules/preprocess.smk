rule groupbarcode:
    input:
        lambda wildcards: sum(
            [
                __import__("glob").glob(
                    "DATA/{Run}/{Barcode}/*.fastq.gz".format(**row)
                )
                for i, row in METADATA[METADATA.Sample == wildcards.sample].iterrows()
            ],
            [],
        ),
    output:
        sample="results/samples/{sample}.fastq.gz",
    params:
        out=lambda wildcards, output: os.path.splitext(output.sample)[0],
    threads: 4
    log:
        "logs/preprocess/groupbarcode/{sample}.log",
    benchmark:
        "logs/benchmark/preprocess/groupbarcode/{sample}.tsv"
    shell:
        """
        conda activate seqkit-2.0.0
        seqkit stats {input} --threads {threads} > {log}
        seqkit seq {input} --threads {threads} > {params.out}
        pigz -p {threads} {params.out}
        conda deactivate
        """


# Following demultiplexing, the Nanopore reads were further processed using Filtlong v0.2.0,
# to remove reads <4000 bp using –min_length 4000 and
# to remove low-quality reads with <80% base call accuracy using –min_mean_q 80 .
rule filtlong:
    input:
        fastq="results/samples/{sample}.fastq.gz",
    output:
        fastq="results/preprocess/filtlong/{sample}.fastq.gz",
    params:
        min_length=1000,
        min_mean_q=90,
    threads: 1
    # log:
    #     "logs/preprocess/filtlong/{sample}.log",
    benchmark:
        "logs/benchmark/preprocess/filtlong/{sample}.tsv"
    shell:
        """
        conda activate filtlong-0.2.1
        filtlong --min_length {params.min_length} --min_mean_q {params.min_mean_q} --verbose {input.fastq} | gzip > {output.fastq}
        conda deactivate
        """


# Porechop v0.2.3 was used to check the reads for residual barcodes and adapters
# using default settings and the flag –min_split_read_size 4000,
# and this was followed by a final round of Filtlong using the described parameters.
rule porechop:
    input:
        fastq="results/preprocess/filtlong/{sample}.fastq.gz",
    output:
        fastq="results/preprocess/porechop/{sample}.fastq.gz",
    params:
        min_split_read_size=1000,
    threads: 4
    log:
        "logs/preprocess/porechop/{sample}.log",
    benchmark:
        "logs/benchmark/preprocess/porechop/{sample}.tsv"
    shell:
        """
        conda activate porechop-0.2.4
        porechop --input {input.fastq} --output {output.fastq} --verbosity 2 --threads {threads} --min_split_read_size {params.min_split_read_size} > {log}
        conda deactivate
        """


rule preprocess_stats:
    input:
        expand(
            "results/{source}/{sample}.fastq.gz",
            source=["samples", "preprocess/filtlong", "preprocess/porechop"],
            sample=SAMPLES,
        ),
    output:
        "results/preprocess/stats.tsv",
    threads: 4
    # log:
    #     "logs/preprocess/preprocess_stats.log",
    benchmark:
        "logs/benchmark/preprocess/preprocess_stats.tsv"
    shell:
        """
        conda activate seqkit-2.0.0
        seqkit stats --tabular {input} --threads {threads} > {output}
        conda deactivate
        """


rule pool_samples:
    input:
        fastq=expand("results/preprocess/porechop/{sample}.fastq.gz", sample=SAMPLES),
    output:
        fastq="results/preprocess/porechop/pool-samples.fastq.gz",
    params:
        out=lambda wildcards, output: os.path.splitext(output.fastq)[0],
    threads: 4
    # log:
    #     "logs/preprocess/groupbarcode/pool-samples.log",
    benchmark:
        "logs/benchmark/preprocess/groupbarcode/pool-samples.tsv"
    shell:
        """
        conda activate seqkit-2.0.0
        seqkit seq {input} --threads {threads} > {params.out}
        pigz -p {threads} {params.out}
        conda deactivate
        """
