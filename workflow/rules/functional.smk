# Prokka v1.14 --meta --kingdom Bacteria or Archaea 80 and Infernal v1.1.2 81 (arguments: cmscan --cut_ga --rfam --nohmmonly --fmt 2)
# were run on the MAGs > 90% complete and <5% contamination, and the reduced genome circular MAGs to identify the 16S, 23S, 5S rRNA, and tRNA genes
rule prodigal:
    input:
        contigs="results/assembly/consensus/{approaches}.fasta",
    output:
        faa="results/functional/prodigal-{approaches}/{approaches}.faa",
        ffn="results/functional/prodigal-{approaches}/{approaches}.ffn",
        gff="results/functional/prodigal-{approaches}/{approaches}.gff",
    threads: 12
    log:
        "logs/functional/prodigal-{approaches}.log",
    benchmark:
        "logs/benchmark/functional/prodigal-{approaches}.tsv"
    shell:
        """
        conda activate prodigal-2.6.3
        prodigal -i {input.contigs} -p meta -c -f gff -o {output.gff} -a {output.faa} -d {output.ffn} &> {log}
        conda deactivate
        """


rule filter_small_gene:
    input:
        ffn="results/functional/prodigal-{approaches}/{approaches}.ffn",
    output:
        ffn="results/functional/prodigal-{approaches}/{approaches}_filtered.ffn",
    params:
        minlen=100,
    threads: 2
    log:
        "logs/functional/filter_small_gene-{approaches}.log",
    benchmark:
        "logs/benchmark/functional/filter_small_gene-{approaches}.tsv"
    shell:
        """
        conda activate seqkit-2.0.0
        seqkit seq --min-len {params.minlen} --threads {threads} --out-file {output.ffn} {input.ffn} 2> {log}
        conda deactivate
        """


rule linclust:
    input:
        ffn="results/functional/prodigal-{approaches}/{approaches}_filtered.ffn",
    output:
        fasta="results/functional/mmseqs-{approaches}/clusterResults_rep_seq.fasta",
        tsv="results/functional/mmseqs-{approaches}/clusterResults_cluster.tsv",
    params:
        tmp=lambda wildcards: __import__("tempfile")
        .TemporaryDirectory(dir="/projet/tmp")
        .name,
        prefix=lambda wildcards, output: output["fasta"].replace("_rep_seq.fasta", ""),
        # covered = 0.9,
        # identity = 0.95
    threads: 20
    log:
        "logs/functional/linclust-{approaches}.log",
    benchmark:
        "logs/benchmark/functional/linclust-{approaches}.tsv"
    shell:
        """
        conda activate mmseqs2-13.45111
        mmseqs easy-linclust --threads {threads} -v 3 {input.ffn} {params.prefix} {params.tmp} &> {log}
        conda deactivate
        """


rule eggnog:
    input:
        fasta="results/functional/mmseqs-{approaches}/clusterResults_rep_seq.fasta",
    output:
        annot="results/functional/eggnog/{approaches}/{approaches}.emapper.annotations",
        hits="results/functional/eggnog/{approaches}/{approaches}.emapper.hits",
        seed="results/functional/eggnog/{approaches}/{approaches}.emapper.seed_orthologs",
    params:
        db="/db/outils/eggnog-mapper-2.1.5/",
        outdir=lambda wildcards, output: os.path.dirname(output[0]),
    threads: 48
    log:
        "logs/functional/eggnog-{approaches}.log",
    benchmark:
        "logs/benchmark/functional/eggnog-{approaches}.tsv"
    shell:
        """
        conda activate eggnog-mapper-2.1.8
        emapper.py --cpu {threads} --override -i {input.fasta} --itype CDS --translate -m diamond --data_dir {params.db} --output_dir {params.outdir} --output {wildcards.approaches} 2> {log}
        conda deactivate
        """
