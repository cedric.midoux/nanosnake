rule align_pool_metamag:
    input:
        reads="results/preprocess/porechop/{sample}.fastq.gz",
        mag="results/reassembl/drep/pool-metamag.fasta",
    output:
        bam="results/reassembl/metamag/{sample}.bam",
    threads: 12
    log:
        "logs/reassembl/align_pool_metamag/{sample}.log",
    benchmark:
        "logs/benchmark/reassembl/align_pool_metamag/{sample}.tsv"
    shell:
        """
        conda activate minimap2-2.22
        conda activate --stack samtools-1.14
        minimap2 -ax map-ont -t {threads} --secondary=no {input.mag} {input.reads} | \
        samtools sort --threads {threads} --output-fmt BAM -o {output.bam} -
        samtools index {output.bam}
        samtools quickcheck {output.bam}
        conda deactivate
        conda deactivate
        """


rule count_metamag:
    input:
        bam="results/reassembl/metamag/{sample}.bam",
    output:
        tsv="results/reassembl/metamag/{sample}.tsv",
    threads: 1
    log:
        "logs/reassembl/count_metamag/{sample}.log",
    benchmark:
        "logs/benchmark/reassembl/count_metamag/{sample}.tsv"
    shell:
        """
        conda activate samtools-1.14
        samtools idxstats --threads {threads} {input.bam} > {output.tsv}
        conda deactivate
        """


rule pool_count_metamag:
    input:
        tsv=expand("results/reassembl/metamag/{sample}.tsv", sample=SAMPLES),
    output:
        tsv="results/reassembl/metamag/pool-metamag.tsv",
    threads: 1
    log:
        "logs/reassembl/pool_count_metamag.log",
    benchmark:
        "logs/benchmark/reassembl/pool_count_metamag.tsv"
    run:
        import os
        import pandas as pd


        def get_table(file):
            sample = os.path.basename(file).replace(".tsv", "")
            df = pd.read_table(file, names=["sequence", "length", "mapped", "unmapped"])
            df["sample"] = sample
            df["metamag"] = df.apply(
                lambda row: "metamag_" + row.sequence.split("_")[1]
                if row.sequence != "*"
                else "UNMAPPED",
                axis=1,
            )
            df["count"] = df.apply(
                lambda row: row.mapped if row.sequence != "*" else row.unmapped, axis=1
            )
            df = df[["sample", "metamag", "sequence", "count"]]
            return df


        pd.concat(get_table(f) for f in input.tsv).to_csv(
            output.tsv, sep="\t", index=False
        )


rule make_phyloseq_metamag:
    input:
        mag="results/reassembl/metamag/pool-metamag.tsv",
        taxo="results/reassembl/gtdb/gtdbtk.summary.tsv",
        meta=config["metadata"],
    output:
        physeq="results/reassembl/metamag/pool-metamag.rds",
    threads: 1
    log:
        "logs/reassembl/make_phyloseq.log",
    benchmark:
        "logs/benchmark/reassembl/make_phyloseq.tsv"
    script:
        "../scripts/make_phyloseq-metamag.R"
