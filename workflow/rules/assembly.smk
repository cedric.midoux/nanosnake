# The quality-processed Nanopore reads were assembled using CANU v1.854, using the following parameters:
# corMinCoverage=0 corOutCoverage=all corMhapSensitivity=high correctedErrorRate=0.105 genomeSize=5m
# corMaxEvidenceCoverageLocal=10 corMaxEvidenceCoverageGlobal=10 oeaMemory=32 redMemory=32 batMemory=200.
# canu FAQ : maxInputCoverage=10000 corOutCoverage=10000 corMhapSensitivity=high corMinCoverage=0 redMemory=32 oeaMemory=32 batMemory=200
rule canu:
    input:
        fastq="results/preprocess/porechop/{sample}.fastq.gz",
    output:
        contigs="results/assembly/canu/{sample}/{sample}.contigs.fasta",
        report="results/assembly/canu/{sample}/{sample}.report",
    params:
        outdir=lambda wildcards, output: os.path.dirname(output.contigs),
        genomeSize="5m",
        corMinCoverage=0,
        corOutCoverage="all",
        corMhapSensitivity="high",
        # correctedErrorRate = 0.105
        # corMaxEvidenceCoverageLocal = 10
        # corMaxEvidenceCoverageGlobal = 10
    threads: 8
    log:
        "logs/assembly/canu/{sample}.log",
    benchmark:
        "logs/benchmark/assembly/canu/{sample}.tsv"
    shell:
        """
        conda activate canu-2.1.1
        canu -p {wildcards.sample} -d {params.outdir} genomeSize={params.genomeSize} corMinCoverage={params.corMinCoverage} corOutCoverage={params.corOutCoverage} corMhapSensitivity={params.corMhapSensitivity} stopOnLowCoverage=0 useGrid=false maxThreads={threads} -nanopore {input.fastq} 2> {log}
        conda deactivate
        """


rule align_prepolish:
    input:
        reads="results/preprocess/porechop/{sample}.fastq.gz",
        contigs="results/assembly/canu/{sample}/{sample}.contigs.fasta",
    output:
        paf=temp("results/align_prepolish/{sample}/{sample}.paf"),
    threads: 12
    log:
        "logs/align/prepolish/{sample}.log",
    benchmark:
        "logs/benchmark/align/prepolish/{sample}.tsv"
    shell:
        """
        conda activate minimap2-2.22
        minimap2 -x map-ont -t {threads} -o {output.paf} {input.contigs} {input.reads} 2> {log}
        conda deactivate
        """


# Nanopore assembly polishing was accomplished using Racon v1.3.3 with the argument --include-unpolished
# for initial correction based on the mapping of the quality processed nanopore reads nusing minimap2 v2.16 57 -x map-ont.
rule polish:
    input:
        fastq="results/preprocess/porechop/{sample}.fastq.gz",
        paf="results/align_prepolish/{sample}/{sample}.paf",
        contigs="results/assembly/canu/{sample}/{sample}.contigs.fasta",
    output:
        contigs=temp("results/assembly/racon/{sample}/{sample}.polish.fasta"),
    threads: 20
    log:
        "logs/assembly/racon/{sample}.log",
    benchmark:
        "logs/benchmark/assembly/racon/{sample}.tsv"
    shell:
        """
        conda activate racon-1.4.20
        racon --include-unpolished --threads {threads} {input.fastq} {input.paf} {input.contigs} > {output.contigs} 2> {log}
        conda deactivate
        """


# This was followed by two rounds of medaka v0.6.5, comprising mini_align, medaka consensus using the r941_flip213 model, and medaka stitch commands.
# For medaka consensus, the assemblies were split into first 10 (for medaka round 1) and then 20 (for medaka round 2) different files
# with the contig headers space separated and provided to the --region flag.
rule medaka1:
    input:
        fastq="results/preprocess/porechop/{sample}.fastq.gz",
        contigs="results/assembly/racon/{sample}/{sample}.polish.fasta",
    output:
        consensus="results/assembly/medaka-1/{sample}/consensus.fasta",
    params:
        outdir=lambda wildcards, output: os.path.dirname(output.consensus),
        model="r941_min_fast_g507",
    threads: 8
    log:
        "logs/assembly/medaka-1/{sample}.log",
    benchmark:
        "logs/benchmark/assembly/medaka-1/{sample}.tsv"
    shell:
        """
        conda activate medaka-1.4.4
        medaka_consensus -i {input.fastq} -d {input.contigs} -t {threads} -o {params.outdir} -m {params.model} > {log}
        conda deactivate
        """


rule medaka2:
    input:
        fastq="results/preprocess/porechop/{sample}.fastq.gz",
        contigs="results/assembly/medaka-1/{sample}/consensus.fasta",
    output:
        consensus="results/assembly/medaka-2/{sample}/consensus.fasta",
    params:
        outdir=lambda wildcards, output: os.path.dirname(output.consensus),
        model="r941_min_fast_g507",
    threads: 8
    log:
        "logs/assembly/medaka-2/{sample}.log",
    benchmark:
        "logs/benchmark/assembly/medaka-2/{sample}.tsv"
    shell:
        """
        conda activate medaka-1.4.4
        medaka_consensus -i {input.fastq} -d {input.contigs} -t {threads} -o {params.outdir} -m {params.model} > {log}
        conda deactivate
        """


rule contigs_rename:
    input:
        consensus="results/assembly/medaka-2/{sample}/consensus.fasta",
    output:
        consensus="results/assembly/consensus/{sample}.fasta",
    threads: 4
    # log:
    #     "logs/assembly/contigsrename/{sample}.log",
    benchmark:
        "logs/benchmark/assembly/contigsrename/{sample}.tsv"
    shell:
        """
        conda activate seqkit-2.0.0
        seqkit replace --pattern '.+' --replacement '{wildcards.sample}_{{nr}}' --nr-width 5 {input.consensus} > {output.consensus}
        conda deactivate
        """


rule assembly_stats:
    input:
        [
            expand("results/assembly/consensus/{sample}.fasta", sample=SAMPLES),
            "results/assembly/consensus/pool-samples.fasta",
        ],
    output:
        "results/assembly/stats.tsv",
    threads: 4
    # log:
    #     "logs/assembly/assembly_stats.log",
    benchmark:
        "logs/benchmark/assembly/assembly_stats.tsv"
    shell:
        """
        conda activate seqkit-2.0.0
        seqkit stats --tabular {input} --threads {threads} > {output}
        conda deactivate
        """


rule pool_consensus:
    input:
        expand("results/assembly/consensus/{sample}.fasta", sample=SAMPLES),
    output:
        "results/assembly/consensus/pool-consensus.fasta",
    threads: 4
    # log:
    #     "logs/assembly/pool-consensus.log",
    benchmark:
        "logs/benchmark/assembly/pool-consensus.tsv"
    shell:
        """
        conda activate seqkit-2.0.0
        seqkit seq {input} --threads {threads} > {output}
        conda deactivate
        """


rule align_postpolish:
    input:
        reads="results/preprocess/porechop/{sample}.fastq.gz",
        contigs="results/assembly/consensus/{sample}.fasta",
    output:
        bam="results/align_postpolish/{sample}/{sample}.bam",
        bai="results/align_postpolish/{sample}/{sample}.bam.bai",
    threads: 12
    log:
        "logs/align/postpolish/{sample}.log",
    benchmark:
        "logs/benchmark/align/postpolish/{sample}.tsv"
    shell:
        """
        conda activate minimap2-2.22
        conda activate --stack samtools-1.14
        minimap2 -ax map-ont -t {threads} {input.contigs} {input.reads} 2> {log} | \
        samtools sort --threads {threads} --output-fmt BAM -o {output.bam} - 2>> {log}
        samtools index {output.bam} 2>> {log}
        samtools quickcheck {output.bam}
        conda deactivate
        conda deactivate
        """


rule quast_consensus:
    input:
        consensus=expand("results/assembly/consensus/{sample}.fasta", sample=SAMPLES),
        #bam = expand("results/align_postpolish/{sample}/{sample}.bam", sample = SAMPLES), 
    output:
        report="results/assembly/quast/report.html",
    params:
        #bam_list = lambda wildcards, input: ','.join(input.bam),
        # metaquast --output-dir {params.outdir} --threads {threads} --gene-finding --rna-finding --conserved-genes-finding --max-ref-number 1000 --bam {params.bam_list} {input.consensus}
        outdir=lambda wildcards, output: os.path.dirname(output.report),
    threads: 24
    log:
        "results/assembly/quast/metaquast.log",
    benchmark:
        "logs/benchmark/assembly/quast.tsv"
    shell:
        """
        conda activate quast-5.0.2
        quast --output-dir {params.outdir} --mgm --threads {threads} {input.consensus} > {log}
        conda deactivate
        """
