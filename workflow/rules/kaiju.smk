rule kaiju:
    input:
        fastq="results/preprocess/porechop/{sample}.fastq.gz",
        nodes="/db/outils/kaiju-2021-03/{db}/nodes.dmp",
        names="/db/outils/kaiju-2021-03/{db}/names.dmp",
        database="/db/outils/kaiju-2021-03/{db}/kaiju_db_{db}.fmi",
    output:
        kaiju="results/kaiju/{sample}/{sample}-{db}.kaiju",
        krona="results/kaiju/{sample}/{sample}-{db}.krona",
        tsv="results/kaiju/{sample}/{sample}-{db}-taxNames.tsv",
        html=temp("results/kaiju/{sample}/{sample}-{db}.html"),
    threads: 24
    log:
        "logs/kaiju/{sample}-{db}.log",
    benchmark:
        "logs/benchmark/kaiju/{sample}-{db}.tsv"
    shell:
        """
        conda activate kaiju-1.8.1
        kaiju -t {input.nodes} -f {input.database} -i {input.fastq} -o {output.kaiju} -z {threads} -v > {log}
        sleep 60
        kaiju2krona -t {input.nodes} -n {input.names} -i {output.kaiju} -o {output.krona} -u -v >> {log}
        kaiju-addTaxonNames -t {input.nodes} -n {input.names} -i {output.kaiju} -o {output.tsv} -r superkingdom,phylum,class,order,family,genus,species -v >> {log}
        conda deactivate
        
        conda activate krona-2.8
        ktImportText -o {output.html} {output.krona} >> {log}
        conda deactivate
        """


rule kronaHTML:
    input:
        expand("results/kaiju/{sample}/{sample}-{{db}}.krona", sample=SAMPLES),
    output:
        html="results/kaiju/krona-{db}.html",
    threads: 4
    log:
        "logs/kaiju/krona-{db}.log",
    benchmark:
        "logs/benchmark/kaiju/krona-{db}.tsv"
    shell:
        """
        conda activate krona-2.8
        ktImportText -o {output.html} {input} > {log}
        conda deactivate
        """


rule kronaTable:
    input:
        kaiju=expand(
            "results/kaiju/{sample}/{sample}-{{db}}-taxNames.tsv", sample=SAMPLES
        ),
        nodes="/db/outils/kaiju-2021-03/{db}/nodes.dmp",
        names="/db/outils/kaiju-2021-03/{db}/names.dmp",
        database="/db/outils/kaiju-2021-03/{db}/kaiju_db_{db}.fmi",
    output:
        tsv="results/kaiju/kronaTable-{db}.tsv",
    params:
        rank="species",
    threads: 4
    log:
        "logs/kaiju/kronaTable-{db}.log",
    benchmark:
        "logs/benchmark/kaiju/kronaTable-{db}.tsv"
    shell:
        """
        conda activate kaiju-1.8.1
        kaiju2table -o {output.tsv} -t {input.nodes} -n {input.names} -r {params.rank} -e -l superkingdom,phylum,class,order,family,genus,species {input.kaiju} > {log}
        conda deactivate
        """
