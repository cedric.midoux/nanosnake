rule split_mag:
    input:
        bam="results/count/mag/all-approaches/{sample}.bam",
    output:
        unknownChrom="results/reassembl/fastq-mag/{sample}/UnknownChrom.bam",
    params:
        outdir=lambda wildcards, output: os.path.dirname(output.unknownChrom),
    log:
        "logs/reassembl/split_mag/{sample}.log",
    benchmark:
        "logs/benchmark/reassembl/split_mag/{sample}.tsv"
    shell:
        """
        conda activate bamutil-1.0.15
        bam splitChromosome --bamout --in {input.bam} --out {params.outdir}/
        conda deactivate
        """


rule bam2fastq:
    input:
        "results/reassembl/fastq-mag/{sample}/UnknownChrom.bam",
    output:
        fastq="results/reassembl/fastq-mag/{sample}/{contig}.fastq",
    params:
        bam="results/reassembl/fastq-mag/{sample}/{contig}.bam",
    threads: 4
    log:
        "logs/reassembl/bam2fastq/{sample}-{contig}.log",
    benchmark:
        "logs/benchmark/reassembl/bam2fastq/{sample}-{contig}.tsv"
    shell:
        """
        conda activate samtools-1.14
        samtools fastq -n {params.bam} > {output.fastq}
        conda deactivate
        """


# mkdir results/reassembl/fastq-mag-len/
# for i in results/reassembl/fastq-mag/*.fastq ; do m=$(basename $i .fastq) ; echo $m ; seqkit fx2tab -nl $i -o results/reassembl/fastq-mag-len/${m}.tsv ; done
#
# library(tidyverse)
#
# files <- fs::dir_ls(path = "results/reassembl/fastq-mag-len", glob = "*.tsv")
# l <- vroom::vroom(files, col_types = "_i", col_names = "len", id = "path") |>
#   mutate(mag=as_factor(basename(path)), .keep = "unused") |>
#   add_count(mag)
#
# p <- ggplot(l, aes(x=len, group=mag, color= n)) +
#   geom_density() +
#   scale_color_gradient(low="blue", high="red")
# ggsave(plot = p ,'mag_density.png',  width = 15, height = 15)
# ggsave(plot = p + xlim(0, 50000),'mag_xlim.png',  width = 15, height = 15)
# ggsave(plot = p + xlim(0, 20000),'mag_xlim2.png',  width = 15, height = 15)
#
# p2 <- ggplot(l, aes(x=len, group=mag, color= n, weight = len)) +
#   geom_density() +
#   scale_color_gradient(low="blue", high="red")
# ggsave(plot = p2 ,'mag_weighted_density.png',  width = 15, height = 15)
# ggsave(plot = p2 + xlim(0, 100000),'weighted_mag_xlim.png',  width = 15, height = 15)


def list_contigs_mag(wildcards):
    import pandas as pd

    txt = checkpoints.contigs_mag.get(
        mag=wildcards.mag, approaches="all-approaches"
    ).output["txt"]
    # txt = "results/binning/drep-all-approaches/mag/{mag}.txt".format(**wildcards)
    return expand(
        "results/reassembl/fastq-mag/{sample}/{contig}.fastq",
        sample="pool-samples",
        contig=pd.read_table(txt, names=["contig"]).contig.tolist(),
    )


rule pool_fastq_mag:
    input:
        list_contigs_mag,
    output:
        fastq="results/reassembl/fastq-mag/{mag,\w+}.fastq",
    threads: 12
    log:
        "logs/reassembl/pool_fastq_mag/{mag}.log",
    benchmark:
        "logs/benchmark/reassembl/pool_fastq_mag/{mag}.tsv"
    shell:
        """
        conda activate seqkit-2.0.0
        seqkit seq {input} --threads {threads} > {output}
        conda deactivate
        """


rule reassembl_canu:
    input:
        fastq="results/reassembl/fastq-mag/{mag}.fastq",
    output:
        contigs="results/reassembl/canu/{mag}/{mag}.contigs.fasta",
        report="results/reassembl/canu/{mag}/{mag}.report",
    params:
        outdir=lambda wildcards, output: os.path.dirname(output.contigs),
        genomeSize="5m",
        corMinCoverage=0,
        corOutCoverage="all",
        corMhapSensitivity="high",
        minInputCoverage=1,
        # correctedErrorRate=0.105
        # corMaxEvidenceCoverageLocal=10
        # corMaxEvidenceCoverageGlobal=10
    threads: 8
    log:
        "logs/reassembl/canu/{mag}.log",
    benchmark:
        "logs/benchmark/reassembl/canu/{mag}.tsv"
    shell:
        """
        conda activate canu-2.1.1
        canu -p {wildcards.mag} -d {params.outdir} genomeSize={params.genomeSize} corMinCoverage={params.corMinCoverage} corOutCoverage={params.corOutCoverage} corMhapSensitivity={params.corMhapSensitivity} minInputCoverage={params.minInputCoverage} stopOnLowCoverage=0 useGrid=false maxThreads={threads} -nanopore {input.fastq} 2> {log}
        conda deactivate
        """


rule reassembl_align_prepolish:
    input:
        reads="results/reassembl/fastq-mag/{mag}.fastq",
        contigs="results/reassembl/canu/{mag}/{mag}.contigs.fasta",
    output:
        paf=temp("results/reassembl/align_prepolish/{mag}/{mag}.paf"),
    threads: 12
    log:
        "logs/reassembl/prepolish/{mag}.log",
    benchmark:
        "logs/benchmark/reassembl/prepolish/{mag}.tsv"
    shell:
        """
        conda activate minimap2-2.22
        minimap2 -x map-ont -t {threads} -o {output.paf} {input.contigs} {input.reads}
        conda deactivate
        """


rule reassembl_polish:
    input:
        fastq="results/reassembl/fastq-mag/{mag}.fastq",
        paf="results/reassembl/align_prepolish/{mag}/{mag}.paf",
        contigs="results/reassembl/canu/{mag}/{mag}.contigs.fasta",
    output:
        contigs="results/reassembl/racon/{mag}/{mag}.polish.fasta",
    threads: 20
    log:
        "logs/reassembl/racon/{mag}.log",
    benchmark:
        "logs/benchmark/reassembl/racon/{mag}.tsv"
    shell:
        """
        conda activate racon-1.4.20
        racon --include-unpolished --threads {threads} {input.fastq} {input.paf} {input.contigs} > {output.contigs}
        conda deactivate
        """


rule reassembl_medaka1:
    input:
        fastq="results/reassembl/fastq-mag/{mag}.fastq",
        contigs="results/reassembl/racon/{mag}/{mag}.polish.fasta",
    output:
        consensus="results/reassembl/medaka-1/{mag}/consensus.fasta",
    params:
        outdir=lambda wildcards, output: os.path.dirname(output.consensus),
        model="r941_min_fast_g507",
    threads: 8
    log:
        "logs/reassembl/medaka-1/{mag}.log",
    benchmark:
        "logs/benchmark/reassembl/medaka-1/{mag}.tsv"
    shell:
        """
        if [ -s {input.contigs} ]
        then
            conda activate medaka-1.4.4
            medaka_consensus -i {input.fastq} -d {input.contigs} -t {threads} -o {params.outdir} -m {params.model}
            conda deactivate
        else
            echo {input.contigs} "is empty" >> {log}
            touch {output}
        fi
        """


rule reassembl_medaka2:
    input:
        fastq="results/reassembl/fastq-mag/{mag}.fastq",
        contigs="results/reassembl/medaka-1/{mag}/consensus.fasta",
    output:
        consensus="results/reassembl/medaka-2/{mag}/consensus.fasta",
    params:
        outdir=lambda wildcards, output: os.path.dirname(output.consensus),
        model="r941_min_fast_g507",
    threads: 8
    log:
        "logs/reassembl/medaka-2/{mag}.log",
    benchmark:
        "logs/benchmark/reassembl/medaka-2/{mag}.tsv"
    shell:
        """
        if [ -s {input.contigs} ]
        then
            conda activate medaka-1.4.4
            medaka_consensus -i {input.fastq} -d {input.contigs} -t {threads} -o {params.outdir} -m {params.model}
            conda deactivate
        else
            echo {input.contigs} "is empty" >> {log}
            touch {output}
        fi
        """


rule reassembl_mag_rename:
    input:
        consensus="results/reassembl/medaka-2/{mag}/consensus.fasta",
    output:
        consensus="results/reassembl/consensus/{mag}.fasta",
    threads: 4
    log:
        "logs/reassembl/contigsrename/{mag}.log",
    benchmark:
        "logs/benchmark/reassembl/contigsrename/{mag}.tsv"
    shell:
        """
        conda activate seqkit-2.0.0
        seqkit replace --pattern '.+' --replacement 'meta{wildcards.mag}_{{nr}}' --nr-width 5 {input.consensus} > {output.consensus}
        conda deactivate
        """


def list_metamag(wildcards):
    import pandas as pd

    stats = checkpoints.stats_mag_names.get(approaches="all-approaches").output["table"]
    return expand(
        "results/reassembl/consensus/{mag}.fasta", mag=pd.read_table(stats).mag.tolist()
    )


rule reassembl_mag_stats:
    input:
        list_metamag,
    output:
        "results/reassembl/stats.tsv",
    threads: 4
    log:
        "logs/reassembl/assembly_stats.log",
    benchmark:
        "logs/benchmark/reassembl/assembly_stats.tsv"
    shell:
        """
        conda activate seqkit-2.0.0
        seqkit stats --tabular {input} --threads {threads} > {output}
        conda deactivate
        """


# rule pool_consensus:
# 	input:
# 		expand("results/assembly/consensus/{sample}.fasta", sample = SAMPLES),
# 	output:
# 		"results/assembly/consensus/pool-consensus.fasta"
# 	params:
# 	threads: 4
# 	log: "logs/assembly/pool-consensus.log"
# 	benchmark: "logs/benchmark/assembly/pool-consensus.tsv"
# 	shell:
# 		"""
# 		conda activate seqkit-2.0.0
# 		seqkit seq {input} --threads {threads} > {output}
# 		conda deactivate
# 		"""


checkpoint reassembl_metabat2:
    input:
        contigs="results/reassembl/consensus/{mag}.fasta",
    output:
        dir=directory("results/reassembl/metabat2/{mag}/"),
        cluster="results/reassembl/metabat2/{mag}/{mag}",
    params:
        minContig=1500,
        maxEdges=500,
    threads: 8
    log:
        "logs/reassembl/metabat2/{mag}.log",
    benchmark:
        "logs/reassembl/benchmark/metabat2/{mag}.tsv"
    shell:
        """
        conda activate metabat2-2.15
        metabat2 --inFile {input.contigs} --outFile {output.dir}/{wildcards.mag} --minContig {params.minContig} --maxEdges {params.maxEdges} --numThreads {threads} --saveCls --unbinned --verbose &> {log}
        conda deactivate
        """


def list_metamag_metabat2(wildcards):
    metamag_path = []
    stats = checkpoints.stats_mag_names.get(approaches="all-approaches").output["table"]
    for mag in pd.read_table(stats).mag.tolist():
        checkpoint_output = checkpoints.reassembl_metabat2.get(mag=mag).output["dir"]
        metamag_path.extend(glob.glob(checkpoint_output + "/" + mag + ".[0-9].fa"))
    return metamag_path


rule reassembl_drep:
    input:
        genomes=list_metamag_metabat2,
    output:
        dir=directory("results/reassembl/drep/"),
        genomes=directory("results/reassembl/drep/dereplicated_genomes/"),
        widb="results/reassembl/drep/data_tables/Widb.csv",
    params:
        # completeness=50,
        # contamination=10,
        completeness=0,
        contamination=10,
        P_ani=0.95,
        S_ani=0.99,
    threads: 24
    log:
        "logs/reassembl/drep.log",
    benchmark:
        "logs/benchmark/reassembl/drep.tsv"
    shell:
        """
        conda activate drep-3.2.2
        dRep dereplicate --S_algorithm fastANI --P_ani {params.P_ani} --S_ani {params.S_ani} --completeness {params.completeness} --contamination {params.contamination} --genomes {input.genomes} --processors {threads} {output.dir} &> {log}
        conda deactivate
        """


localrules:
    stats_metamag_temp,
    stats_metamag_names,
    metamag_rename,
    pool_metamag,


rule stats_metamag_temp:
    input:
        genome="results/reassembl/drep/dereplicated_genomes/",
    output:
        temp("results/reassembl/drep/temp_stats_metamag.tsv"),
    params:
        list_mag=lambda wildcards, input: __import__("glob").glob(
            "{}/*.fa".format(input.genome)
        ),
    threads: 1
    benchmark:
        "logs/benchmark/reassembl/drep.temp_stats_metamag.tsv"
    shell:
        """
        conda activate seqkit-2.0.0
        seqkit stats {params.list_mag} --tabular --threads {threads} > {output}
        conda deactivate
        """


checkpoint stats_metamag_names:
    input:
        table="results/reassembl/drep/temp_stats_metamag.tsv",
    output:
        table="results/reassembl/drep/stats_metamag.tsv",
    threads: 1
    benchmark:
        "logs/benchmark/reassembl/drep.stats_metamag.tsv"
    run:
        import pandas as pd

        df = pd.read_table(input.table)
        df["metamag"] = "metamag_" + df.sum_len.rank(ascending=False, method="first").astype("int").astype("str").str.zfill(3)
        df.to_csv(output.table, sep="\t", index=False)


rule metamag_rename:
    input:
        table="results/reassembl/drep/stats_metamag.tsv",
    output:
        "results/reassembl/drep/metamag/{metamag}.fasta",
    params:
        metamag_path=lambda wildcards, input: __import__("pandas").read_table(input.table).set_index("metamag").file[wildcards.metamag],
    threads: 1
    benchmark:
        "logs/benchmark/reassembl/metamagrename/{metamag}.tsv"
    shell:
        """
        conda activate seqkit-2.0.0
        seqkit replace --pattern '.+' --replacement '{wildcards.metamag}_{{nr}}' --nr-width 5 {params.metamag_path} > {output}
        conda deactivate
        """


def metaMAGs(wildcards):
    return (
        pd.read_table(checkpoints.stats_metamag_names.get(**wildcards).output["table"])
        .metamag.sort_values()
        .unique()
        .tolist()
    )


def list_metamag(wildcards):
    return expand("results/reassembl/drep/metamag/{metamag}.fasta", metamag=metaMAGs(wildcards))


checkpoint contigs_metamag:
    input:
        fasta="results/reassembl/drep/metamag/{metamag}.fasta",
    output:
        txt="results/reassembl/drepmag/{metamag}.txt",
    threads: 1
    benchmark:
        "logs/benchmark/reassembl/contigs_metamag/{metamag}.tsv"
    shell:
        """
        conda activate seqkit-2.0.0
        seqkit seq --name {input.fasta} --out-file {output.txt}
        conda deactivate
        """


rule pool_metamag:
    input:
        list_metamag,
    output:
        "results/reassembl/drep/pool-metamag.fasta",
    threads: 1
    benchmark:
        "logs/benchmark/reassembl/pool-metamag.tsv"
    shell:
        """
        conda activate seqkit-2.0.0
        seqkit seq {input} --threads {threads} > {output}
        conda deactivate
        """


rule checkm2_metamag:
    input:
        pool="results/reassembl/drep/pool-metamag.fasta",
    output:
        report="results/reassembl/checkm2_metamag/quality_report.tsv",
    params:
        mag=lambda wildcards: "results/reassembl/drep/metamag/",
        outdir=lambda wildcards, output: os.path.dirname(output.report),
    threads: 12
    log:
        "logs/reassembl/checkm2_metamag.log",
    benchmark:
        "logs/benchmark/reassembl/checkm2_metamag.tsv"
    shell:
        """
        conda activate checkm2-0.1.3
        checkm2 predict --threads 12 --extension .fasta --input {params.mag} --output-directory {params.outdir} &> {log}
        conda deactivate
        """
