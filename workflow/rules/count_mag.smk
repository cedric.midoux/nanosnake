rule align_pool_mag:
    input:
        reads="results/preprocess/porechop/{sample}.fastq.gz",
        mag="results/binning/drep-{approaches}/pool-mag.fasta",
    output:
        bam="results/count/mag/{approaches}/{sample}.bam",
    threads: 12
    log:
        "logs/count/align_pool_mag/{approaches}-{sample}.log",
    benchmark:
        "logs/benchmark/count/align_pool_mag/{approaches}-{sample}.tsv"
    shell:
        """
        conda activate minimap2-2.22
        conda activate --stack samtools-1.14
        minimap2 -ax map-ont -t {threads} --secondary=no {input.mag} {input.reads} | \
        samtools sort --threads {threads} --output-fmt BAM -o {output.bam} -
        samtools index {output.bam}
        samtools quickcheck {output.bam}
        conda deactivate
        conda deactivate
        """


rule count_mag:
    input:
        bam="results/count/mag/{approaches}/{sample}.bam",
    output:
        tsv="results/count/mag/{approaches}/{sample}.tsv",
    threads: 1
    log:
        "logs/count/count_mag/{approaches}-{sample}.log",
    benchmark:
        "logs/benchmark/count/count_mag/{approaches}-{sample}.tsv"
    shell:
        """
        conda activate samtools-1.14
        samtools idxstats --threads {threads} {input.bam} > {output.tsv}
        conda deactivate
        """


rule pool_count_mag:
    input:
        tsv=expand(
            "results/count/mag/{{approaches}}/{sample}.tsv", sample=SAMPLES
        ),
    output:
        tsv="results/count/mag/pool-mag-{approaches}.tsv",
    threads: 1
    log:
        "logs/count/pool_count_mag/{approaches}.log",
    benchmark:
        "logs/benchmark/count/pool_count_mag/{approaches}.tsv"
    run:
        import os
        import pandas as pd


        def get_table(file):
            sample = os.path.basename(file).replace(".tsv", "")
            df = pd.read_table(file, names=["sequence", "length", "mapped", "unmapped"])
            df["sample"] = sample
            df["mag"] = df.apply(
                lambda row: "mag_" + row.sequence.split("_")[1]
                if row.sequence != "*"
                else "UNMAPPED",
                axis=1,
            )
            df["count"] = df.apply(
                lambda row: row.mapped if row.sequence != "*" else row.unmapped, axis=1
            )
            df = df[["sample", "mag", "sequence", "count"]]
            return df


        pd.concat(get_table(f) for f in input.tsv).to_csv(
            output.tsv, sep="\t", index=False
        )


rule make_phyloseq:
    input:
        mag="results/count/mag/pool-mag-{approaches}.tsv",
        taxo="results/annotation/gtdb-{approaches}/gtdbtk.summary.tsv",
        meta=config["metadata"],
    output:
        physeq="results/count/mag/pool-mag-{approaches}.rds",
    threads: 1
    log:
        "logs/count/make_phyloseq/{approaches}.log",
    benchmark:
        "logs/benchmark/count/make_phyloseq/{approaches}.tsv"
    script:
        "../scripts/make_phyloseq-mag.R"
