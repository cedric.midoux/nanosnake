rule gtdb_metamag:
    input:
        wait=rules.pool_metamag.output,
    output:
        expand(
            "results/reassembl/gtdb/gtdbtk.{db}.summary.tsv",
            db=["bac120", "ar53"],
        ),
    params:
        genomes="results/reassembl/drep/metamag/",
        outdir=lambda wildcards, output: os.path.dirname(output[0]),
        min=10,
    threads: 48
    log:
        "logs/taxo_metamag/gtdb.log",
    benchmark:
        "logs/benchmark/taxo_metamag/gtdb.tsv"
    shell:
        """
        conda activate gtdbtk-2.2.3
        gtdbtk classify_wf --genome_dir {params.genomes} --out_dir {params.outdir} --extension fasta --skip_ani_screen --min_perc_aa {params.min} --cpus {threads} --force &> {log}
        conda deactivate
        """


rule gtdb_combine_metamag:
    input:
        summary=expand(
            "results/reassembl/gtdb/gtdbtk.{db}.summary.tsv",
            db=["bac120", "ar53"],
        ),
    output:
        tsv="results/reassembl/gtdb/gtdbtk.summary.tsv",
    threads: 1
    benchmark:
        "logs/benchmark/taxo_metamag/gtdb_combine.tsv"
    run:
        import pandas as pd

        pd.concat(
            [pd.read_table(file, index_col=0) for file in input.summary], axis=0
        ).to_csv(output.tsv, sep="\t")
