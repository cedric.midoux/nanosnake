rule align_pool_gene:
    input:
        reads="results/preprocess/porechop/{sample}.fastq.gz",
        gene="results/functional/mmseqs-{approaches}/clusterResults_rep_seq.fasta",
    output:
        bam="results/count/gene/{approaches}/{sample}.bam",
    threads: 12
    log:
        "logs/count/align_pool_gene/{approaches}-{sample}.log",
    benchmark:
        "logs/benchmark/count/align_pool_gene/{approaches}-{sample}.tsv"
    shell:
        """
        conda activate minimap2-2.22
        conda activate --stack samtools-1.14
        minimap2 -ax map-ont -t {threads} --secondary=yes -p 0.8 {input.gene} {input.reads} | \
        samtools sort --threads {threads} --output-fmt BAM -o {output.bam} -
        samtools index {output.bam}
        samtools quickcheck {output.bam}
        conda deactivate
        conda deactivate
        """


rule count_gene:
    input:
        bam="results/count/gene/{approaches}/{sample}.bam",
    output:
        tsv="results/count/gene/{approaches}/{sample}.tsv",
    threads: 1
    log:
        "logs/count/count_gene/{approaches}-{sample}.log",
    benchmark:
        "logs/benchmark/count/count_gene/{approaches}-{sample}.tsv"
    shell:
        """
        conda activate samtools-1.14
        samtools idxstats --threads {threads} {input.bam} > {output.tsv}
        conda deactivate
        """


rule pool_count_gene:
    input:
        tsv=expand(
            "results/count/gene/{{approaches}}/{sample}.tsv", sample=SAMPLES
        ),
    output:
        tsv="results/count/gene/pool-gene-{approaches}.tsv",
    threads: 1
    log:
        "logs/count/pool_count_gene/{approaches}.log",
    benchmark:
        "logs/benchmark/count/pool_count_gene/{approaches}.tsv"
    run:
        import os
        import pandas as pd


        def get_table(file):
            sample = os.path.basename(file).replace(".tsv", "")
            df = pd.read_table(file, names=["gene", "length", "mapped", "unmapped"])
            df["sample"] = sample
            df.loc[df.gene == "*", "gene"] = "UNMAPPED"
            df["count"] = df.apply(
                lambda row: row.mapped if row.gene != "UNMAPPED" else row.unmapped,
                axis=1,
            )
            df = df[["sample", "gene", "count"]]
            return df


        pd.concat(get_table(f) for f in input.tsv).to_csv(
            output.tsv, sep="\t", index=False
        )


rule countKO:
    input:
        tsv="results/count/gene/pool-gene-{approaches}.tsv",
        kegg="results/functional/eggnog/{approaches}/{approaches}.emapper.annotations",
        meta=config["metadata"],
    output:
        tsv="results/count/gene/sample-{approaches}-KO.tsv",
    threads: 4
    log:
        "logs/count/gene/sample-{approaches}-KO.log",
    benchmark:
        "logs/benchmark/count/gene/sample-{approaches}-KO.tsv"
    script:
        "../scripts/countKO.R"
