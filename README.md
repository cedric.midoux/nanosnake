# nanosnake

```sh
# connection à un noeud de calcul
qlogin -pe thread 8 -q long.q

# activation environnement conda Snakemeake
conda activate snakemake-7.5.0

# devreouiller l'espace de travail
snakemake --unlock

# execution à blanc
snakemake --printshellcmds --dryrun --rerun-incomplete 

# execution réelle
snakemake --printshellcmds --rerun-incomplete --cores 8 --restart-times 5 --use-conda

# soumission de jobs
snakemake --printshellcmds --rerun-incomplete --jobs 100 --restart-times 5 --use-conda --cluster-config config/cluster.yaml --cluster 'qsub -V -cwd -R y -N {rule} -q {cluster.queue} -pe thread {threads} -e logs/sge/ -o logs/sge/' --latency-wait 60

# le tout sur un job
qsub -cwd -V -N nanosnake -pe thread 4 -e LOG/ -o LOG/ -q infinit.q -b y "conda activate snakemake-7.5.0 && snakemake --printshellcmds --jobs 500 --restart-times 5 --use-conda --cluster-config config/cluster.yaml --cluster 'qsub -V -cwd -R y -N {rule} -q {cluster.queue} -pe thread {threads} -e logs/sge/ -o logs/sge/' --latency-wait 60 --rerun-incomplete && conda deactivate"
```
